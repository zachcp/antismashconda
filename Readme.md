# Conda + Antismash


```
# install miniconda2 and conda-build suite
https://conda.io/miniconda.html

conda install conda-build

# get the packageable verision of antismash
cd ~
git clone https://zachcp@bitbucket.org/zachcp/antismash.git
cd antismash
git checkout setuppy

# get this repo
cd ~
git clone https://zachcp@bitbucket.org/zachcp/antismashconda.git
cd antismashconda
conda build .

#upload
 anaconda upload /data/miniconda/conda-bld/linux-64/antismash-4.0.0rc1-py27h221592b_0.tar.bz2


```

# Basic use
```
conda install -c zachcp antismash=4.0.0rc1

run_antismash <seq.fasta> --minimal  -v

# note only minimal will work
```